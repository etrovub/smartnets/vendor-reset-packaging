#!/bin/sh -ex

VERSION=0.0.1

export DEBIAN_FRONTEND=noninteractive
apt-get -y update
apt-get -y install devscripts dkms debhelper

cat > framework.conf << EOF
source_tree="$PWD"
#dkms_tree="$PWD/dkms"
EOF

rm -rf vendor-reset-$VERSION
cp -r vendor-reset vendor-reset-$VERSION

dkms add -m vendor-reset/$VERSION --dkmsframework framework.conf
dkms mkdsc -m vendor-reset/$VERSION --dkmsframework framework.conf

rm -rf build/vendor-reset-dkms-$VERSION build/venodr-reset-dkms_$VERSION*
mkdir -p build

cp /var/lib/dkms/vendor-reset/$VERSION/dsc/* build/
cd build/
dpkg-source -x vendor-reset-dkms_$VERSION.dsc
cd vendor-reset-dkms-$VERSION
debuild -uc -us
