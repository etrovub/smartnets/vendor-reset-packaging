# vendor-reset: Ubuntu packages

Debian packages for [`vendor-reset`](https://github.com/gnif/vendor-reset),
built with Gitlab CI

Downloads:
- [Ubuntu 20.04](https://gitlab.com/etrovub/smartnets/vendor-reset-packaging/-/jobs/artifacts/main/browse/build?job=build:ubuntu:20.04)
- [Ubuntu 21.10](https://gitlab.com/etrovub/smartnets/vendor-reset-packaging/-/jobs/artifacts/main/browse/build?job=build:ubuntu:21.10)
- [Ubuntu 22.04](https://gitlab.com/etrovub/smartnets/vendor-reset-packaging/-/jobs/artifacts/main/browse/build?job=build:ubuntu:22.04)

Packages for Fedora and CentOS would be in scope of this project.
